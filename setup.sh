#!/usr/bin/env bash

if [[ $EUID == 0 ]]; then
   echo "This script must be run as non-root user inside docker group. All staff will be done automatically, but 'sudo' required."
   sudo groupadd docker
   sudo gpasswd -a $USER docker
   echo "Please re-login to your system to activate changes."
   exit 1
fi

mkdir ~/tpcc

git clone https://gitlab.com/Lipovsky/tpcc-course-2019.git ~/tpcc/tpcc
docker-compose -f docker-compose.yml up -d --build --force-recreate
docker exec -it --user $(id -u):$(id -g) tpcc-image sh -c "/root/tpcc/init.sh"
