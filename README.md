## **tpcc-course** install repo

### Requirements:
- `docker`
- `docker-compose`
- `bash`

### Installation:
- Run `bash setup.sh`

### Intellij integration:
- Make sure container is running:
  ```
  docker container list | grep tpcc
  ```
- In Clion:
  1. Setup remote host:
  ```
  username: user
  password: password
  ```
  ![Setup remote host](images/remote-host-setup.png)
  ![Insert credentials](images/credentials.png)
  2. Choose `Remote host` toolchain:
  ![Setup toolchain](images/toolchain.png)
